package controller;

import java.util.ArrayList;
import model.Produto;

public class ControleProduto {
    
    private ArrayList<Produto> listaProdutos;
    
    public ControleProduto() {
        listaProdutos = new ArrayList<Produto>();
    }
    
    public ArrayList<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(ArrayList<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
    public void adicionarProduto(Produto umProduto){
        listaProdutos.add(umProduto);
    }
    public Produto pesquisarProduto (String umNome) {
        for (Produto umProduto : listaProdutos) {
            if (umProduto.getNome().equals(umNome)) return umProduto;
        }
        return null;
    }
    public void exibirProdutos(){
        for(Produto umProduto: listaProdutos){
            System.out.println(umProduto.getNome());
        }
    }
    
    
   
    
    
}
