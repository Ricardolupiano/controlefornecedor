/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

public class controleFornecedor {
    
    private ArrayList<PessoaJuridica> listaFornecedoresJuridica;
    private ArrayList<PessoaFisica> listaFornecedoresFisica;
    
    public controleFornecedor(){
    listaFornecedoresJuridica = new ArrayList<PessoaJuridica>();
    listaFornecedoresFisica = new ArrayList<PessoaFisica>();
    }

    public ArrayList<PessoaJuridica> getListaFornecedoresJuridica() {
        return listaFornecedoresJuridica;
    }
    public ArrayList<PessoaFisica> getListaFornecedoresFisica() {
        return listaFornecedoresFisica;
    }
    
    public void adicionarFisica(PessoaFisica fornecedor){
        listaFornecedoresFisica.add(fornecedor);
    }
    public void adicionarJuridica(PessoaJuridica fornecedor){
        listaFornecedoresJuridica.add(fornecedor);
    }
    public void removerFisica(PessoaFisica umaPessoa){
        listaFornecedoresFisica.remove(umaPessoa);
    }
    public void removerJuridica(PessoaJuridica umaPessoa){
        listaFornecedoresJuridica.remove(umaPessoa);
    }
    
    public PessoaFisica pesquisarFisica(String Nome){
        for(PessoaFisica f: listaFornecedoresFisica){
            if(f.getNome().equalsIgnoreCase(Nome)){
                return f;
            }
        }
        return null;
    }
    public PessoaJuridica pesquisarJuridico(String Nome){
        for(PessoaJuridica j: listaFornecedoresJuridica){
            if(j.getNome().equalsIgnoreCase(Nome))
                return j;
        }
        return null;
    }
    
    
}
