package model;

import java.util.ArrayList;

public class Fornecedor {
    protected String nome=null;
    protected ArrayList<String> telefones= null;
    protected Endereco localizacao=null;
    protected ArrayList<Produto> produtos= null;
    
    public Fornecedor(String nome, String telefone, Endereco umEndereco, Produto umProduto){
        this.nome = nome;
        localizacao= umEndereco;
        telefones= new ArrayList<String>();
        telefones.add(telefone);
        produtos= new ArrayList<Produto>();
        produtos.add(umProduto);
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefone) {
        telefones.add(telefone);
    }

    public Endereco getEndereco() {
        return localizacao;
    }

    public void setEndereco(Endereco endereco) {
        this.localizacao = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
}
