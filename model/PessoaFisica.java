/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
public class PessoaFisica extends Fornecedor{
    private String cpf;
    
    public PessoaFisica(String nome, String telefone,String cpf,Endereco localizacao,Produto umProduto){
        super(nome,telefone,localizacao,umProduto);
        this.cpf= cpf;
    }

    public String getCpf(){
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
