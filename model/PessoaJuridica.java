package model;

public class PessoaJuridica extends Fornecedor {
    private String cnpj;
    private String razaoSocial;
    
//construtores
    public PessoaJuridica (String nome, String telefone, String cnpj,String razaoSocial,Endereco localizacao, Produto umProduto){
        super(nome,telefone,localizacao,umProduto);
        this.cnpj=cnpj;
        this.razaoSocial = razaoSocial;
    }
    
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    
   
      
}
