
package model;

public class Produto {
    
    private String nome;
    private String descricao;
    private double valorCompra;
    private double valorVenda;
    private double quantidadeEstoque;
    
    public Produto(String nome, String descricao, double valorCompra, double valorVenda, double quantidadeEstoque){
        this.nome=nome;
        this.descricao=descricao;
        this.valorCompra=valorCompra;
        this.valorVenda=valorVenda;
        this.quantidadeEstoque=quantidadeEstoque;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }

    public double getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(double quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
}
