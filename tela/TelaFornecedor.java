/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tela;

import controller.ControleProduto;
import controller.controleFornecedor;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import model.Endereco;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;

public class TelaFornecedor extends javax.swing.JFrame {
    Produto umProduto;
    ControleProduto umControleProduto = new ControleProduto();
    boolean modoAlteracao;
    boolean novoProduto;
    boolean modoAlteracaoPessoaFisica;
    boolean modoAlteracaoPessoaJuridica;
    boolean modoAlteracaoProduto;
    controleFornecedor umControleFornecedor = new controleFornecedor();
    Fornecedor umFornecedor;
    PessoaFisica umaPessoaFisica;
    Produto p;
    PessoaJuridica umaPessoaJuridica;
    int x=0;
    int y=0;
    int tabela=1;
    
    
    private void exibirInformacao(String info){
        JOptionPane.showMessageDialog(this, info,"Atenção" ,JOptionPane.INFORMATION_MESSAGE);
    }
    public void limparCamposProduto(){
        //Produtos
        this.jTextFieldNomeProduto.setText("");
        this.jTextFieldDescricaoProduto.setText("");
        this.jTextFieldValorCompra.setText("0.0");
        this.jTextFieldValorVenda.setText("0.0");
        this.jTextFieldQuantidadeProduto.setText("0.0");
    }
    public void  limparCamposGeral () {
        //informacoes gerais
        this.jTextFieldNome.setText("");
        this.jTextFieldCpf.setText("");
        this.jTextFieldTelefone.setText("");
        this.jTextFieldCnpj.setText("");
        this.jTextFieldRazaoSocial.setText("");
        //endereco
        this.jTextFieldLogradouro.setText("");
        this.jTextFieldNumero.setText("");
        this.jTextFieldBairro.setText("");
        this.jTextFieldCidade.setText("");
        this.jTextFieldEstado.setText("");
        this.jTextFieldPais.setText("");
        this.jTextFieldComplemento.setText("");
        //Produtos
        this.jTextFieldNomeProduto.setText("");
        this.jTextFieldDescricaoProduto.setText("");
        this.jTextFieldValorCompra.setText("0.0");
        this.jTextFieldValorVenda.setText("0.0");
        this.jTextFieldQuantidadeProduto.setText("0.0");
    }
    
    public void PeencherCamposProduto(Produto umProduto){
        this.jTextFieldNomeProduto.setText(umProduto.getNome());
        this.jTextFieldDescricaoProduto.setText(umProduto.getDescricao());
        this.jTextFieldValorCompra.setText(String.valueOf(umProduto.getValorCompra()));
        this.jTextFieldValorVenda.setText(String.valueOf(umProduto.getValorVenda()));
        this.jTextFieldQuantidadeProduto.setText(String.valueOf(umProduto.getQuantidadeEstoque()));   
    }
    public void  PreencherCamposGeralPessoaFisica (PessoaFisica umaPessoa) {
        //informacoes gerais
        this.jTextFieldNome.setText(umaPessoa.getNome());
        this.jTextFieldCpf.setText(umaPessoa.getCpf());
        this.jTextFieldTelefone.setText(umaPessoa.getTelefones().get(0));
        
        //endereco
        this.jTextFieldLogradouro.setText(umaPessoa.getEndereco().getLogradouro());
        this.jTextFieldNumero.setText(umaPessoa.getEndereco().getNumero());
        this.jTextFieldBairro.setText(umaPessoa.getEndereco().getBairro());
        this.jTextFieldCidade.setText(umaPessoa.getEndereco().getCidade());
        this.jTextFieldEstado.setText(umaPessoa.getEndereco().getEstado());
        this.jTextFieldPais.setText(umaPessoa.getEndereco().getPais());
        this.jTextFieldComplemento.setText(umaPessoa.getEndereco().getComplemento());
        carregarListaProdutos();
    }
    public void  PreencherCamposGeralPessoaJuridica (PessoaJuridica umaPessoa) {
        //informacoes gerais
        this.jTextFieldNome.setText(umaPessoa.getNome());
        //this.jTextFieldCpf.setText(umaPessoa.getCpf());
        this.jTextFieldTelefone.setText(umaPessoa.getTelefones().get(0));
        this.jTextFieldCnpj.setText(umaPessoa.getCnpj());
        this.jTextFieldRazaoSocial.setText(umaPessoa.getRazaoSocial());
        //endereco
        this.jTextFieldLogradouro.setText(umaPessoa.getEndereco().getLogradouro());
        this.jTextFieldNumero.setText(umaPessoa.getEndereco().getNumero());
        this.jTextFieldBairro.setText(umaPessoa.getEndereco().getBairro());
        this.jTextFieldCidade.setText(umaPessoa.getEndereco().getCidade());
        this.jTextFieldEstado.setText(umaPessoa.getEndereco().getEstado());
        this.jTextFieldPais.setText(umaPessoa.getEndereco().getPais());
        this.jTextFieldComplemento.setText(umaPessoa.getEndereco().getComplemento());
        carregarListaProdutos();
    }
    public void habilitarDesabilitarCampos () {
        jTextFieldNome.setEnabled(modoAlteracao);
        jTextFieldCpf.setEnabled(modoAlteracaoPessoaFisica);
        jTextFieldTelefone.setEnabled(modoAlteracao);
        jTextFieldCnpj.setEnabled(modoAlteracaoPessoaJuridica);
        jTextFieldRazaoSocial.setEnabled(modoAlteracaoPessoaJuridica);
        //endereco
        jTextFieldLogradouro.setEnabled(modoAlteracao);
        jTextFieldNumero.setEnabled(modoAlteracao);
        jTextFieldBairro.setEnabled(modoAlteracao);
        jTextFieldCidade.setEnabled(modoAlteracao);
        jTextFieldEstado.setEnabled(modoAlteracao);
        jTextFieldPais.setEnabled(modoAlteracao);
        jTextFieldComplemento.setEnabled(modoAlteracao);
        //Produtos
        jTextFieldNomeProduto.setEnabled(modoAlteracaoProduto);
        jTextFieldDescricaoProduto.setEnabled(modoAlteracaoProduto);
        jTextFieldValorCompra.setEnabled(modoAlteracaoProduto);
        jTextFieldValorVenda.setEnabled(modoAlteracaoProduto);
        jTextFieldQuantidadeProduto.setEnabled(modoAlteracaoProduto);
        
    }
    private void carregarLista() {
        if(tabela==1){
            ArrayList<PessoaFisica> listaPessoaFisica2 = umControleFornecedor.getListaFornecedoresFisica();
            DefaultTableModel model = (DefaultTableModel) jTableListas.getModel();
            model.setRowCount(0);
            for (PessoaFisica b : listaPessoaFisica2) {
                model.addRow(new String[]{b.getNome(), b.getCpf()});
            }
            jTableListas.setModel(model);
        }
        else if(tabela==2)
        {
            ArrayList<PessoaJuridica> listaPessoaJuridica = umControleFornecedor.getListaFornecedoresJuridica();
            DefaultTableModel model = (DefaultTableModel) jTableListas.getModel();
            model.setRowCount(0);
            for (PessoaJuridica b : listaPessoaJuridica) {
                model.addRow(new String[]{b.getNome(), b.getCnpj()});
            }
            jTableListas.setModel(model);
            
        }
    }
    private void carregarListaProdutosVazia(){
            DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
            model.setRowCount(0);
            //model.addRow(new String[]{"",""});
            
            jTableProdutos.setModel(model);
    }
    private void carregarListaProdutos() {
        if(tabela==1){
            ArrayList<Produto> listaProduto = umaPessoaFisica.getProdutos();
            DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
            model.setRowCount(0);
            for (Produto b : listaProduto) {
                model.addRow(new String[]{b.getNome(), Double.toString(b.getQuantidadeEstoque())});
            }
            jTableProdutos.setModel(model);
        }
        else if(tabela==2)
        {
            ArrayList<Produto> listaProduto = umaPessoaJuridica.getProdutos();
            DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
            model.setRowCount(0);
            for (Produto b : listaProduto) {
                model.addRow(new String[]{b.getNome(), Double.toString(b.getQuantidadeEstoque())});
            }
            jTableProdutos.setModel(model);
            
        }
    }
    public TelaFornecedor() {
        initComponents();
        modoAlteracao= false;
        modoAlteracaoPessoaFisica=false;
        modoAlteracaoPessoaJuridica=false;
        habilitarDesabilitarCampos();
        limparCamposGeral();
        jButtonOk.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonPesquisar.setEnabled(false);
        jButtonSair.setEnabled(false);
        jButtonSalvar.setEnabled(false);
        jButtonEditar.setEnabled(false);
        jButtonConfirmarEdicao.setEnabled(false);
        jButtonSalvarProduto.setEnabled(false);
        jButtonNovoProduto.setEnabled(false);
        jButtonExcluirProduto.setEnabled(false);
        jButtonEditarProduto.setEnabled(false);
        jButtonConfirmarEdicaoProduto.setEnabled(false);
        this.jTableListas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.jTableProdutos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jButtonNovaPessoaFisica = new javax.swing.JButton();
        jButtonNovaPessoaJuridica = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonSair = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldTelefone = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldCpf = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldCnpj = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldBairro = new javax.swing.JTextField();
        jTextFieldCidade = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldEstado = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldPais = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldComplemento = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabelNomeProduto = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jTextFieldQuantidadeProduto = new javax.swing.JTextField();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jTextFieldDescricaoProduto = new javax.swing.JTextField();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jButtonSalvarProduto = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableProdutos = new javax.swing.JTable();
        jButtonExcluirProduto = new javax.swing.JButton();
        jButtonNovoProduto = new javax.swing.JButton();
        jButtonEditarProduto = new javax.swing.JButton();
        jButtonConfirmarEdicaoProduto = new javax.swing.JButton();
        jButtonOk = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonConfirmarEdicao = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListas = new javax.swing.JTable();
        jComboBoxSelecaoTabela = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonNovaPessoaFisica.setText("Nova Pessoa Fisica");
        jButtonNovaPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovaPessoaFisicaActionPerformed(evt);
            }
        });

        jButtonNovaPessoaJuridica.setText("Nova Pessoa Juridica");
        jButtonNovaPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovaPessoaJuridicaActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jButtonSair.setText("Sair");
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome:");

        jTextFieldNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNomeActionPerformed(evt);
            }
        });

        jLabel2.setText("Telefone:");

        jLabel3.setText("Cpf:");

        jLabel4.setText("Cnpj:");

        jTextFieldCnpj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCnpjActionPerformed(evt);
            }
        });

        jLabel5.setText("Razão Social:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldRazaoSocial))
                .addContainerGap(489, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4, jLabel5});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(112, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4, jLabel5});

        jTabbedPane1.addTab("Informações gerais", jPanel1);

        jLabel6.setText("Logradouro:");

        jTextFieldLogradouro.setText(" ");

        jLabel7.setText("Numero:");

        jLabel8.setText("Bairro:");

        jLabel9.setText("Cidade:");

        jLabel10.setText("Estado:");

        jLabel11.setText("Pais:");

        jLabel12.setText("Complemento:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jTextFieldBairro, jTextFieldCidade, jTextFieldEstado, jTextFieldLogradouro, jTextFieldNumero, jTextFieldPais});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel11, jLabel6, jLabel7, jLabel8, jLabel9});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12))
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jTextFieldBairro, jTextFieldCidade, jTextFieldEstado, jTextFieldLogradouro, jTextFieldNumero, jTextFieldPais});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel10, jLabel11, jLabel6, jLabel7, jLabel8, jLabel9});

        jTabbedPane1.addTab("Endereco", jPanel2);

        jLabelNomeProduto.setText("Nome Produto:");

        jLabel14.setText("Descrição Produto:");

        jLabel15.setText("Valor de Compra:");

        jLabel16.setText("Valor de Venda:");

        jLabel17.setText("Quantidade em Estoque:");

        jTextFieldValorVenda.setText(" ");

        jTextFieldQuantidadeProduto.setText(" ");

        jTextFieldValorCompra.setText(" ");

        jTextFieldDescricaoProduto.setText(" ");

        jTextFieldNomeProduto.setText(" ");

        jButtonSalvarProduto.setText("Salvar Produto");
        jButtonSalvarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarProdutoActionPerformed(evt);
            }
        });

        jTableProdutos.setModel(new javax.swing.table.DefaultTableModel
            (
                null,
                new String [] {
                    "Nome Produto", "Estoque"
                }
            )
            {
                @Override
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTableProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTableProdutosMouseClicked(evt);
                }
            });
            jScrollPane2.setViewportView(jTableProdutos);

            jButtonExcluirProduto.setText("Excluir Produto");
            jButtonExcluirProduto.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonExcluirProdutoActionPerformed(evt);
                }
            });

            jButtonNovoProduto.setText("Novo Produto");
            jButtonNovoProduto.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonNovoProdutoActionPerformed(evt);
                }
            });

            jButtonEditarProduto.setText("Editar Produto");
            jButtonEditarProduto.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonEditarProdutoActionPerformed(evt);
                }
            });

            jButtonConfirmarEdicaoProduto.setText("Confirmar Edicao");
            jButtonConfirmarEdicaoProduto.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonConfirmarEdicaoProdutoActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
            jPanel4.setLayout(jPanel4Layout);
            jPanel4Layout.setHorizontalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel17)
                        .addComponent(jLabel16)
                        .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabelNomeProduto))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                            .addComponent(jTextFieldValorCompra)
                            .addComponent(jTextFieldDescricaoProduto))
                        .addComponent(jTextFieldQuantidadeProduto)
                        .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(61, 61, 61)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButtonExcluirProduto)
                        .addComponent(jButtonSalvarProduto)
                        .addComponent(jButtonNovoProduto)
                        .addComponent(jButtonEditarProduto)
                        .addComponent(jButtonConfirmarEdicaoProduto))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(510, 510, 510))
            );

            jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabelNomeProduto});

            jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButtonConfirmarEdicaoProduto, jButtonEditarProduto, jButtonExcluirProduto, jButtonNovoProduto, jButtonSalvarProduto});

            jPanel4Layout.setVerticalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldValorVenda))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldQuantidadeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(122, 122, 122))
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldDescricaoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonExcluirProduto))
                        .addComponent(jLabel14))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(39, 39, 39))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jButtonNovoProduto)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButtonEditarProduto)))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jButtonConfirmarEdicaoProduto)
                    .addGap(111, 111, 111))
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeProduto)
                            .addComponent(jTextFieldNomeProduto)
                            .addComponent(jButtonSalvarProduto))))
            );

            jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel17, jLabelNomeProduto});

            jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButtonConfirmarEdicaoProduto, jButtonEditarProduto, jButtonExcluirProduto, jButtonNovoProduto, jButtonSalvarProduto});

            jTabbedPane1.addTab("Produtos", jPanel4);

            jButtonOk.setText("OK");
            jButtonOk.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonOkActionPerformed(evt);
                }
            });

            jButtonEditar.setText("Editar");
            jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonEditarActionPerformed(evt);
                }
            });

            jButtonConfirmarEdicao.setText("ConfirmarEdicao");
            jButtonConfirmarEdicao.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonConfirmarEdicaoActionPerformed(evt);
                }
            });

            jTableListas.setModel(new javax.swing.table.DefaultTableModel
                (
                    null,
                    new String [] {
                        "Nome", "CPF/CNPJ"
                    }
                )
                {
                    @Override
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jTableListas.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        jTableListasMouseClicked(evt);
                    }
                });
                jScrollPane1.setViewportView(jTableListas);

                jComboBoxSelecaoTabela.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pessoa Fisica", "Pessoa Juridica", " " }));
                jComboBoxSelecaoTabela.addItemListener(new java.awt.event.ItemListener() {
                    public void itemStateChanged(java.awt.event.ItemEvent evt) {
                        jComboBoxSelecaoTabelaItemStateChanged(evt);
                    }
                });
                jComboBoxSelecaoTabela.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jComboBoxSelecaoTabelaActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
                jPanel3.setLayout(jPanel3Layout);
                jPanel3Layout.setHorizontalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonOk))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                        .addComponent(jButtonNovaPessoaFisica)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonNovaPessoaJuridica)
                                        .addGap(87, 87, 87)
                                        .addComponent(jButtonPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonEditar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonConfirmarEdicao))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 869, Short.MAX_VALUE))))
                                .addGap(0, 90, Short.MAX_VALUE)))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonExcluir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSalvar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonSair))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jComboBoxSelecaoTabela, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(14, 14, 14))
                );
                jPanel3Layout.setVerticalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButtonNovaPessoaFisica)
                                    .addComponent(jButtonNovaPessoaJuridica)
                                    .addComponent(jButtonPesquisar)
                                    .addComponent(jButtonSalvar)
                                    .addComponent(jButtonExcluir)
                                    .addComponent(jButtonSair)
                                    .addComponent(jButtonOk)
                                    .addComponent(jButtonEditar)
                                    .addComponent(jButtonConfirmarEdicao))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(jComboBoxSelecaoTabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(348, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovaPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovaPessoaFisicaActionPerformed
        this.limparCamposGeral();
        this.modoAlteracaoPessoaFisica = true;
        this.modoAlteracao=true;
        this.modoAlteracaoPessoaJuridica=false;
        this.modoAlteracaoProduto=true;
        this.habilitarDesabilitarCampos();
        this.novoProduto = true;
        jTextFieldNome.requestFocus();
        jButtonNovaPessoaFisica.setEnabled(false);
        jButtonNovaPessoaJuridica.setEnabled(false);
        jButtonPesquisar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonSair.setEnabled(true);
        carregarListaProdutosVazia();
        jButtonExcluirProduto.setEnabled(false);
        jButtonNovoProduto.setEnabled(false);
        jButtonEditarProduto.setEnabled(false);
        
        
    }//GEN-LAST:event_jButtonNovaPessoaFisicaActionPerformed

    private void jButtonNovaPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovaPessoaJuridicaActionPerformed
        this.limparCamposGeral();
        this.modoAlteracaoPessoaFisica = false;
        this.modoAlteracao=true;
        this.modoAlteracaoPessoaJuridica=true;
        this.modoAlteracaoProduto=true;
        this.habilitarDesabilitarCampos();
        this.novoProduto = true;
        jTextFieldNome.requestFocus();
        jButtonNovaPessoaFisica.setEnabled(false);
        jButtonNovaPessoaJuridica.setEnabled(false);
        jButtonPesquisar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(true);
        jButtonSair.setEnabled(true);
        jButtonSalvarProduto.setEnabled(false);
        jButtonExcluirProduto.setEnabled(false);
        jButtonNovoProduto.setEnabled(false);
        jButtonEditarProduto.setEnabled(false);
        carregarListaProdutosVazia();
    }//GEN-LAST:event_jButtonNovaPessoaJuridicaActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        String umNome = jTextFieldNome.getText();
        String tel = jTextFieldTelefone.getText();
        //endereco
        String umLogradouro=jTextFieldLogradouro.getText();
        String umNumero=jTextFieldNumero.getText();
        String umBairro=jTextFieldBairro.getText();
        String umaCidade=jTextFieldCidade.getText();
        String umEstado=jTextFieldEstado.getText();
        String umPais=jTextFieldPais.getText();
        String umComplemento=jTextFieldComplemento.getText();
        //Produtos
        String umNomeProduto=jTextFieldNomeProduto.getText();
        String umaDescricao=jTextFieldDescricaoProduto.getText();
        double umValor = Double.parseDouble(jTextFieldValorCompra.getText());
        double umValorVenda= Double.parseDouble(jTextFieldValorVenda.getText());
        double umaQuantidade=Double.parseDouble(jTextFieldQuantidadeProduto.getText());
        umProduto = new Produto(umNomeProduto,umaDescricao,umValor,umValorVenda,umaQuantidade);
        Endereco umEndereco = new Endereco(umLogradouro,umNumero,umBairro,umaCidade,umEstado,umPais,umComplemento);
        if(("".equals(jTextFieldNomeProduto.getText()))){
        exibirInformacao("Digite um nome para o Produto!");
        }
        else if(("".equals(jTextFieldNome.getText()))){
            exibirInformacao("Digite um nome para o Fornecedor!");
        }
        else{
            if(modoAlteracaoPessoaFisica==true){
            String umCpf = jTextFieldCpf.getText();
            ArrayList<PessoaFisica> lista = umControleFornecedor.getListaFornecedoresFisica();
            for(PessoaFisica f:lista)
                if(f.getNome().equalsIgnoreCase(jTextFieldNome.getText()))
                    y=5;
            if(y==5)
                exibirInformacao("Fornecedor ja cadastrado");
            else{
            PessoaFisica UmaPessoaFisica = new PessoaFisica(umNome,tel,umCpf,umEndereco,umProduto);
            umControleFornecedor.adicionarFisica(UmaPessoaFisica);
            this.modoAlteracaoPessoaFisica = false;
            this.modoAlteracao=false;
            this.modoAlteracaoPessoaJuridica=false;
            habilitarDesabilitarCampos();
            limparCamposGeral();
            jButtonPesquisar.setEnabled(true);
            //tabela=1;
            }
            }
            else if (modoAlteracaoPessoaJuridica==true){
            String umCnpj=jTextFieldCnpj.getText();
            String umaRazao=jTextFieldRazaoSocial.getText();
            ArrayList<PessoaJuridica> lista = umControleFornecedor.getListaFornecedoresJuridica();
            for(PessoaJuridica j:lista)
                if(j.getNome().equalsIgnoreCase(jTextFieldNome.getText()))
                    y=5;
            if(y==5)
                exibirInformacao("Fornecedor ja cadastrado");
            else{
            PessoaJuridica UmaPessoaJuridica = new PessoaJuridica(umNome,tel,umCnpj,umaRazao,umEndereco,umProduto);
            umControleFornecedor.adicionarJuridica(UmaPessoaJuridica);
            //tabela=2;
            }
            }
            limparCamposGeral();
            this.modoAlteracaoPessoaFisica = false;
            this.modoAlteracao=false;
            this.modoAlteracaoPessoaJuridica=false;
            this.modoAlteracaoProduto=false;
            habilitarDesabilitarCampos();
            jButtonPesquisar.setEnabled(true);
            jButtonNovaPessoaFisica.setEnabled(true);
            jButtonNovaPessoaJuridica.setEnabled(true);
            jButtonSair.setEnabled(false);
            jButtonSalvar.setEnabled(false);
            this.carregarLista();
            //this.carregarListaProdutos();
        }
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        this.limparCamposGeral();
        this.modoAlteracaoPessoaFisica = false;
        this.modoAlteracao=false;
        this.modoAlteracaoPessoaJuridica=false;
        this.modoAlteracaoProduto=false;
        this.habilitarDesabilitarCampos();
        this.novoProduto = true;
        jButtonNovaPessoaFisica.setEnabled(true);
        jButtonNovaPessoaJuridica.setEnabled(true);
        jButtonPesquisar.setEnabled(true);
        jButtonExcluir.setEnabled(true);
        jButtonSalvar.setEnabled(true);
        jButtonSair.setEnabled(true);
        jButtonEditar.setEnabled(false);
        jButtonConfirmarEdicao.setEnabled(false);
        jButtonSalvarProduto.setEnabled(true);
        jButtonExcluirProduto.setEnabled(false);
        jButtonConfirmarEdicaoProduto.setEnabled(false);
        jButtonEditarProduto.setEnabled(false);
        jButtonNovoProduto.setEnabled(false);
        limparCamposGeral();
        carregarListaProdutosVazia();
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
        this.modoAlteracaoPessoaFisica = false;
        this.modoAlteracao=false;
        this.modoAlteracaoPessoaJuridica=false;
        this.modoAlteracaoProduto=false;
        this.habilitarDesabilitarCampos();
        jTextFieldNome.setEnabled(true);
        jTextFieldNome.requestFocus();
        jButtonOk.setEnabled(true);
        jButtonNovaPessoaFisica.setEnabled(false);
        jButtonNovaPessoaJuridica.setEnabled(false);
        jButtonPesquisar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvar.setEnabled(false);
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        String umNome = jTextFieldNome.getText();
        if((umControleFornecedor.pesquisarFisica(umNome)==null)&& umControleFornecedor.pesquisarJuridico(umNome)==null){
            limparCamposGeral();
            String mensagem = "Fonecedor nao encontrado!";
            exibirInformacao(mensagem);
        }
        else if(umControleFornecedor.pesquisarFisica(umNome) == null){
            umaPessoaJuridica = umControleFornecedor.pesquisarJuridico(umNome);
            PreencherCamposGeralPessoaJuridica(umaPessoaJuridica);
            x=1;
            jButtonExcluir.setEnabled(true);
        }
        else if (umControleFornecedor.pesquisarJuridico(umNome) == null){
            umaPessoaFisica = umControleFornecedor.pesquisarFisica(umNome);
            PreencherCamposGeralPessoaFisica(umaPessoaFisica);
            x=2;
            jButtonExcluir.setEnabled(true);
        }
        jButtonOk.setEnabled(false);
        jButtonSair.setEnabled(true);
        jButtonEditar.setEnabled(true);
        jTextFieldNome.setEnabled(false);
            
    }//GEN-LAST:event_jButtonOkActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
       if(x==1){
           umControleFornecedor.removerJuridica(umaPessoaJuridica);
       }
       else if(x==2){
           umControleFornecedor.removerFisica(umaPessoaFisica);
       }
       limparCamposGeral();
       habilitarDesabilitarCampos();
       jButtonExcluir.setEnabled(false);
       jButtonSair.setEnabled(false);
       jButtonNovaPessoaFisica.setEnabled(true);
       jButtonNovaPessoaJuridica.setEnabled(true);
       jButtonPesquisar.setEnabled(true);
       jButtonEditar.setEnabled(false);
       this.carregarLista();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jTextFieldCnpjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCnpjActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCnpjActionPerformed

    private void jTextFieldNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNomeActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        if(x==1){
        modoAlteracao= true;
        modoAlteracaoPessoaFisica=false;
        modoAlteracaoPessoaJuridica=true;
        this.modoAlteracaoProduto=false;
        habilitarDesabilitarCampos();
        }
        else if(x==2){
        modoAlteracao= true;
        modoAlteracaoPessoaFisica=true;
        modoAlteracaoPessoaJuridica=false;
        this.modoAlteracaoProduto=false;
        habilitarDesabilitarCampos(); 
        }
        jButtonConfirmarEdicao.setEnabled(true);
        jButtonEditar.setEnabled(false);
        jButtonExcluir.setEnabled(false);
        jButtonSalvarProduto.setEnabled(true);
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonConfirmarEdicaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfirmarEdicaoActionPerformed
       if(x==1){
           umaPessoaJuridica.setNome(jTextFieldNome.getText());
           umaPessoaJuridica.getTelefones().set(0, jTextFieldTelefone.getText());
           umaPessoaJuridica.setCnpj(jTextFieldCnpj.getText());
           umaPessoaJuridica.setRazaoSocial(jTextFieldRazaoSocial.getText());
           umaPessoaJuridica.getEndereco().setLogradouro(jTextFieldLogradouro.getText());
           umaPessoaJuridica.getEndereco().setNumero(jTextFieldNumero.getText());
           umaPessoaJuridica.getEndereco().setBairro(jTextFieldBairro.getText());
           umaPessoaJuridica.getEndereco().setCidade(jTextFieldCidade.getText());
           umaPessoaJuridica.getEndereco().setEstado(jTextFieldEstado.getText());
           umaPessoaJuridica.getEndereco().setPais(jTextFieldPais.getText());
            
       }
       else if(x==2){
           umaPessoaFisica.setNome(jTextFieldNome.getText());
           umaPessoaFisica.getTelefones().set(0, jTextFieldTelefone.getText());
           umaPessoaFisica.setCpf(jTextFieldCpf.getText());
           umaPessoaFisica.getEndereco().setLogradouro(jTextFieldLogradouro.getText());
           umaPessoaFisica.getEndereco().setNumero(jTextFieldNumero.getText());
           umaPessoaFisica.getEndereco().setBairro(jTextFieldBairro.getText());
           umaPessoaFisica.getEndereco().setCidade(jTextFieldCidade.getText());
           umaPessoaFisica.getEndereco().setEstado(jTextFieldEstado.getText());
           umaPessoaFisica.getEndereco().setPais(jTextFieldPais.getText());
       }
       jButtonConfirmarEdicao.setEnabled(false);
       jButtonEditar.setEnabled(false);
       modoAlteracao= false;
       modoAlteracaoPessoaFisica=false;
       modoAlteracaoPessoaJuridica=false;
       modoAlteracaoProduto=false;
       habilitarDesabilitarCampos();
       limparCamposGeral();
       jButtonSair.setEnabled(false);
       jButtonNovaPessoaFisica.setEnabled(true);
       jButtonNovaPessoaJuridica.setEnabled(true);
       jButtonPesquisar.setEnabled(true);
       this.carregarLista();
       
    }//GEN-LAST:event_jButtonConfirmarEdicaoActionPerformed

    private void jComboBoxSelecaoTabelaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxSelecaoTabelaItemStateChanged
        if(jComboBoxSelecaoTabela.getSelectedItem().toString().equals("Pessoa Fisica")){
            tabela=1;
            this.carregarLista();
            
        }
            
        else{
            this.carregarLista();
            tabela=2;
        }
            
    }//GEN-LAST:event_jComboBoxSelecaoTabelaItemStateChanged

    private void jComboBoxSelecaoTabelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSelecaoTabelaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxSelecaoTabelaActionPerformed

    private void jTableListasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListasMouseClicked
        DefaultTableModel model = (DefaultTableModel) jTableListas.getModel();
        String nome = (String) model.getValueAt(jTableListas.getSelectedRow(), 0);
        if(umControleFornecedor.pesquisarFisica(nome) == null){
            umaPessoaJuridica = umControleFornecedor.pesquisarJuridico(nome);
            PreencherCamposGeralPessoaJuridica(umaPessoaJuridica);
            x=1;
            jButtonExcluir.setEnabled(true);
        }
        else if (umControleFornecedor.pesquisarJuridico(nome) == null){
            umaPessoaFisica = umControleFornecedor.pesquisarFisica(nome);
            PreencherCamposGeralPessoaFisica(umaPessoaFisica);
            x=2;
            jButtonExcluir.setEnabled(true);
        }
        jButtonEditar.setEnabled(true);
        modoAlteracaoProduto=false;
        habilitarDesabilitarCampos();
        jButtonSair.setEnabled(true);
        jButtonNovoProduto.setEnabled(true);
        jButtonExcluirProduto.setEnabled(false);
        jButtonEditarProduto.setEnabled(false);
        jButtonConfirmarEdicaoProduto.setEnabled(false);
    }//GEN-LAST:event_jTableListasMouseClicked

    @SuppressWarnings("empty-statement")
    private void jButtonSalvarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarProdutoActionPerformed
 if("".equals(jTextFieldNome.getText())){
            limparCamposGeral();
            String mensagem = "Fonecedor nao encontrado!";
            exibirInformacao(mensagem);
        }
 else{ 
     if(("".equals(jTextFieldNomeProduto.getText()))){
          exibirInformacao("Digite um nome para o Produto!");
      } 
     else{
        if(x==1){
        String umNomeProduto=jTextFieldNomeProduto.getText();
        String umaDescricao=jTextFieldDescricaoProduto.getText();
        double umValor = Double.parseDouble(jTextFieldValorCompra.getText());
        double umValorVenda= Double.parseDouble(jTextFieldValorVenda.getText());
        double umaQuantidade=Double.parseDouble(jTextFieldQuantidadeProduto.getText());
        ArrayList<Produto> listaP = umaPessoaJuridica.getProdutos();
        for(Produto p : listaP)
            if(p.getNome().equalsIgnoreCase(jTextFieldNomeProduto.getText())){
                y=1;
            }
        if(y==1)
            exibirInformacao("Produto ja Cadastrado!");
        else{
        umProduto = new Produto(umNomeProduto,umaDescricao,umValor,umValorVenda,umaQuantidade);
        listaP.add(umProduto);
        }
       }
       else if(x==2){
        String umNomeProduto=jTextFieldNomeProduto.getText();
        String umaDescricao=jTextFieldDescricaoProduto.getText();
        double umValor = Double.parseDouble(jTextFieldValorCompra.getText());
        double umValorVenda= Double.parseDouble(jTextFieldValorVenda.getText());
        double umaQuantidade=Double.parseDouble(jTextFieldQuantidadeProduto.getText());
        ArrayList<Produto> listaP = umaPessoaFisica.getProdutos();
        for(Produto p : listaP)
            if(p.getNome().equalsIgnoreCase(jTextFieldNomeProduto.getText())){
                y=1;
            }
        if(y==1)
            exibirInformacao("Produto ja Cadastrado!");
        else{
        umProduto = new Produto(umNomeProduto,umaDescricao,umValor,umValorVenda,umaQuantidade);
        listaP.add(umProduto); 
        }
       }
     
       limparCamposProduto();
       carregarListaProdutos();
       jButtonSalvarProduto.setEnabled(false);
       modoAlteracaoProduto=false;
       habilitarDesabilitarCampos();
      }
      }
    }//GEN-LAST:event_jButtonSalvarProdutoActionPerformed

    private void jTableProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProdutosMouseClicked
       DefaultTableModel model = (DefaultTableModel) jTableProdutos.getModel();
        String nome = (String) model.getValueAt(jTableProdutos.getSelectedRow(), 0);
        jButtonEditarProduto.setEnabled(true);
        jButtonExcluirProduto.setEnabled(true);
        jButtonNovoProduto.setEnabled(false);
        if(x==1){
            ArrayList<Produto> lista =umaPessoaJuridica.getProdutos();
            for(Produto p:lista){
                if(p.getNome().equals(nome)){
                    umProduto=p;
                    PeencherCamposProduto(p);
                }      
            }
        }
        else if (x==2){
            ArrayList<Produto> lista =umaPessoaFisica.getProdutos();
            for(Produto p:lista){
                if(p.getNome().equals(nome)){
                    PeencherCamposProduto(p);
                    umProduto=p;
                }      
            }
        }
    }//GEN-LAST:event_jTableProdutosMouseClicked

    private void jButtonExcluirProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirProdutoActionPerformed
        if(x==1){
            ArrayList<Produto> lista =umaPessoaJuridica.getProdutos();
            lista.remove(umProduto);           
        }
        else if (x==2){
            ArrayList<Produto> lista =umaPessoaFisica.getProdutos();
            lista.remove(umProduto);
        }
        carregarListaProdutos();
        limparCamposProduto();
    }//GEN-LAST:event_jButtonExcluirProdutoActionPerformed

    private void jButtonNovoProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoProdutoActionPerformed
       limparCamposProduto();
       modoAlteracaoProduto=true;
       habilitarDesabilitarCampos();
       jTextFieldNomeProduto.requestFocus();
       jButtonSalvarProduto.setEnabled(true);
       jButtonNovoProduto.setEnabled(false);
    }//GEN-LAST:event_jButtonNovoProdutoActionPerformed

    private void jButtonEditarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarProdutoActionPerformed
       modoAlteracaoProduto=true;
       habilitarDesabilitarCampos();
       jButtonSalvarProduto.setEnabled(false);
       jButtonExcluirProduto.setEnabled(false);
       jButtonNovoProduto.setEnabled(false);
       jButtonExcluirProduto.setEnabled(false);
       jButtonConfirmarEdicaoProduto.setEnabled(true);
       jButtonEditarProduto.setEnabled(false);
    }//GEN-LAST:event_jButtonEditarProdutoActionPerformed

    private void jButtonConfirmarEdicaoProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfirmarEdicaoProdutoActionPerformed
       
    umProduto.setNome(jTextFieldNomeProduto.getText());
    umProduto.setDescricao(jTextFieldDescricaoProduto.getText());
    umProduto.setQuantidadeEstoque(Double.parseDouble(jTextFieldQuantidadeProduto.getText()));
    umProduto.setValorCompra(Double.parseDouble(jTextFieldValorCompra.getText()));
    umProduto.setValorVenda(Double.parseDouble(jTextFieldValorVenda.getText())); 
    limparCamposProduto();
    jButtonConfirmarEdicaoProduto.setEnabled(false);
    modoAlteracaoProduto=false;
    jButtonNovoProduto.setEnabled(true);
    habilitarDesabilitarCampos();
    carregarListaProdutos();
       
      
    }//GEN-LAST:event_jButtonConfirmarEdicaoProdutoActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonConfirmarEdicao;
    private javax.swing.JButton jButtonConfirmarEdicaoProduto;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonEditarProduto;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonExcluirProduto;
    private javax.swing.JButton jButtonNovaPessoaFisica;
    private javax.swing.JButton jButtonNovaPessoaJuridica;
    private javax.swing.JButton jButtonNovoProduto;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSair;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonSalvarProduto;
    private javax.swing.JComboBox jComboBoxSelecaoTabela;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelNomeProduto;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableListas;
    private javax.swing.JTable jTableProdutos;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldDescricaoProduto;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldQuantidadeProduto;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldTelefone;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables
}
